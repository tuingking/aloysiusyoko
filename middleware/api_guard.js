const JWT = require('../plugins/jwt');

module.exports = async (req, res, next) => {
    try {
        const token = req.headers.authorization;
        if (!token) throw new Error('token not provided');
        try {
            const payload = await JWT.verify(token);
        } catch (err) {
            throw new Error('Not Authorized');
        }
        return next();
    } catch (err) {
        return next(err);
    }
};
