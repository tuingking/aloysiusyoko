/**
 * Error Middleware
 */
module.exports = (error, req, res, next) => {
    const errCode = error.code || 500;
    const errObj = error.err || {};
    res.status(errCode).json({
        error: {
            message: error.message,
            obj: errObj
        }
    });
}