const App = require('./app');

App.init();
App.registerRoutes('users');
App.registerRoutes('authentication');

App.run();
