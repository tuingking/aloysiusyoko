const JWT = require('jsonwebtoken');

const jwtConfig = {
    secret: 'S3CR3TZ',
    expired: 3600
};

exports.create = async credentials => JWT.sign(credentials, jwtConfig.secret, { expiresIn: jwtConfig.expired });

exports.verify = async token => JWT.verify(token, jwtConfig.secret);

module.exports = exports;
