# Simple App

Base url: https://aloysiusyoko.herokuapp.com

## Authorization

Header:

Authorization : [TOKEN]

## Endpoint:

[POST] `/token`:  get token


[GET] `/users`:  get all users

[POST] `/users`:  create users
body
```json
{
	"username": "test",
	"account_number": "123",
	"email_address": "123@123.com",
	"identity_number": "123"
}
```
[GET] `/users/<id>`:  get user by id

[GET] `/users/account/<accountNumber>`:  get user by account number

[GET] `/users/idn/<identityNumber>`:  get user by identity number

[POST] `/users/<id>`:  update user

[DELETE] `/users/<id>`:  delete user

