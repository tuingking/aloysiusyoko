const redisUrl = 'redis://h:pd4ba445c497f65f53243da2dc77b63db469a9c10f4612df037d0b02814b4b629@ec2-3-213-42-236.compute-1.amazonaws.com:10849';
const redis = require('redis');
const bluebird = require('bluebird');
const client = redis.createClient(process.env.REDIS_URL || redisUrl);

bluebird.promisifyAll(redis.RedisClient.prototype);
bluebird.promisifyAll(redis.Multi.prototype);

client.on("error", function (err) {
    console.log("[REDIS] Error: " + err);
});

client.on("connect", function () {
    console.log("[REDIS] Connected.");
});

module.exports = client;