var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var userSchema = new Schema({
  userName: String,
  accountNumber: String,
  emailAddress: String,
  identityNumber: String
});

var Users = mongoose.model('Users', userSchema);

module.exports = Users;