const mongoose = require('mongoose');
const config = require('./../config');
const Users = require('./user');

const uri = "mongodb+srv://root:password1234@cluster0-0lqxz.mongodb.net/test?retryWrites=true";
mongoose.connect(config.getDbConnectionString(), { useNewUrlParser: true, useFindAndModify: false })
    .catch((err) => {
        console.log('mongo error');
        console.log(err);
    });

module.exports = {
    Users
}