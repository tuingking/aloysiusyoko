const Status = require('http-status');

class UserError extends Error {
    constructor(...args) {
        super(...args);
        Error.captureStackTrace(this, UserError);
    }

    static NotFound(err) {
        this.message = 'User Not Found';
        this.code = Status.NOT_FOUND;
        this.error = err;
        return this;
    }

    static Unknown(err) {
        this.message = 'Unknown';
        this.code = Status.INTERNAL_SERVER_ERROR;
        this.error = err;
        return this;
    }
}

module.exports = UserError;
