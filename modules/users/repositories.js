const { Users } = require('../../database').mongodb;
const redisClient = require('./../../database/redis');
class UserRepo {

    async findAll() {
        // check cache
        const usersCache = await redisClient.multi().hgetall('users').execAsync();
        if (usersCache) {
            const users = usersCache.map((user) => JSON.parse(Object.values(user)));
            return users;
        }

        const users = await Users.find();
        return users;
    }

    async findById(id) {
        // check cache
        const userCache = await redisClient.multi().hget('users', id).execAsync();
        if (userCache[0] !== null) {
            return JSON.parse(userCache);
        }

        const user = await Users.findById({ _id: id });
        return user;
    }

    async findByAccountNumber(accountNumber) {
        const user = await Users.find({ accountNumber });
        return user;
    }

    async findByIdentityNumber(identityNumber) {
        const user = await Users.find({ identityNumber });
        return user;
    }

    async create({ userName, accountNumber, emailAddress, identityNumber }) {
        const newUser = Users({
            userName,
            accountNumber,
            emailAddress,
            identityNumber,
        });
        const user = await newUser.save();

        // Cache
        await redisClient.hset('users', user._id.toString(), JSON.stringify(user));

        return user;
    }

    async update({ id, userName, accountNumber, emailAddress, identityNumber }) {
        const user = {
            userName,
            accountNumber,
            emailAddress,
            identityNumber,
        };
        await Users.findOneAndUpdate(id, user);

        // Update Cache
        await redisClient.hset('users', id.toString(), JSON.stringify(Object.assign(user, { _id: id.toString() })));

        return this.findById(id);
    }

    async delete(id) {
        await Users.findOneAndDelete(id).catch(err => {
            throw err;
        });

        // Delete cache
        await redisClient.hdel('users', id.toString());
    }
}

module.exports = new UserRepo();
