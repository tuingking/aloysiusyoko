const UserRepo = require('./repositories');
const { Response } = require('../../utils/http_response');
const UserError = require('./errors');

exports.getAll = async (req, res, next) => {
    try {
        const { page, limit } = req.query;
        const users = await UserRepo.findAll();

        return Response(res, users);
    } catch (err) {
        next(err);
    }
}

exports.getOne = async (req, res, next) => {
    try {
        const user = await UserRepo.findById(req.params.id);

        if (!user) {
            throw UserError.Unknown();
        }
        return Response(res, user);
    } catch (err) {
        next(err);
    }
}

exports.getOneByAccountNumber = async (req, res, next) => {
    try {
        console.log('req.params.accountNumber');
        console.log(req.params.accountNumber);
        const user = await UserRepo.findByAccountNumber(req.params.accountNumber);
        if (!user) {
            throw UserError.Unknown();
        }
        return Response(res, user);
    } catch (err) {
        next(err);
    }
}

exports.getOneByIdentityNumber = async (req, res, next) => {
    try {
        const user = await UserRepo.findByIdentityNumber(req.params.identityNumber);
        if (!user) {
            throw UserError.Unknown();
        }
        return Response(res, user);
    } catch (err) {
        next(err);
    }
}

exports.create = async (req, res, next) => {
    try {
        const body = req.body;
        console.log(body);
        const newUser = await UserRepo.create({
            userName: body.username,
            accountNumber: body.account_number,
            emailAddress: body.email_address,
            identityNumber: body.identity_number,
        });

        return Response(res, newUser);
    } catch (err) {
        next(err);
    }
}

exports.update = async (req, res, next) => {
    try {
        const user = await UserRepo.findById(req.params.id);
        if (!user) {
            throw UserError.NotFound();
        }
        
        const body = req.body;
        const updatedUser = await UserRepo.update({
            id: req.params.id,
            userName: body.username,
            accountNumber: body.account_number,
            emailAddress: body.email_address,
            identityNumber: body.identity_number,
        });

        return Response(res, updatedUser);
    } catch (err) {
        next(err);
    }
}

exports.remove = async (req, res, next) => {
    try {
        const user = await UserRepo.findById(req.params.id);
        if (!user) {
            throw UserError.NotFound();
        }

        await UserRepo.delete(req.params.id)
        return Response(res);
    } catch (err) {
        next(err);
    }
}




module.exports = exports;
