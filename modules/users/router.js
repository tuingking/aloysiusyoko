const Users = require('./methods');
const ApiGuard = require('../../middleware/api_guard');

module.exports = (app) => {
    app.get('/users', ApiGuard, Users.getAll);
    app.post('/users', ApiGuard, Users.create);

    app.get('/users/:id', ApiGuard, Users.getOne);
    app.get('/users/account/:accountNumber', ApiGuard, Users.getOneByAccountNumber);
    app.get('/users/idn/:identityNumber', ApiGuard, Users.getOneByIdentityNumber);
    app.get('/users/:id', ApiGuard, Users.getOne);
    app.post('/users/:id', ApiGuard, Users.update);
    app.delete('/users/:id', ApiGuard, Users.remove);
};
