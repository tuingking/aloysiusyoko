const Authentication = require('./methods');

module.exports = (app) => {
    app.post('/token', Authentication.generateToken);
};
