const AuthService = require('./services');
const { Response } = require('../../utils/http_response');

exports.generateToken = async (req, res, next) => {
    try {
        // const body = req.body;
        // const { account_number: accountNumber } = body;
        const token = await AuthService.generateToken();

        return Response(res, { token });
    } catch (err) {
        console.log('err');
        console.log(err);
        next(err);
    }
}


module.exports = exports;
