const JWT = require('./../../plugins/jwt');
class AuthService {

    async generateToken(accountNumber) {
        const token = await JWT.create({ timestamp: new Date().getTime() });
        return token;
    }
}

module.exports = new AuthService();
