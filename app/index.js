const path = require('path');
const compression = require('compression');
const express = require('express');
const bodyParser = require('body-parser');
const errorMiddleware = require('../middleware/error');

let app = null;

exports.getApp = () => {
    if(!app) {
        throw new Error('[App] Unknown App');
    }
    return app;
}

exports.init = () => {
    app = express();

    // Middleware
    app.use(compression());
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({ extended: false }));

    return app;
}

exports.registerRoutes = (moduleName) => {
    if (!app) {
        throw new Error('[App] Unknown App');
    }

    const absPath = path.join(__dirname, '../modules', moduleName, 'router.js');
    require(absPath)(app);
}

exports.registerMiddleware = (middleware) => {
    app.use(middleware);
}

exports.run = () => {
    app.use(errorMiddleware);
    
    const port = process.env.PORT || 5000;
    app.listen(port, () => {
        console.log(`App running on port ${port}`);
    });
}

module.exports = exports;
