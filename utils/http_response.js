const Status = require('http-status');

const Response = (res, data = {}, code = Status.OK) => {
    const resTemplate = {
        data
    }
    return res.status(code).json(resTemplate);
}

module.exports = {
    Status,
    Response
}
