exports.calcOffset = (page, limit) => {
    return limit * (page - 1);
}

exports.calcTotalPages = (count, limit) => {
    return Math.ceil(count / limit);
}

module.exports = exports;